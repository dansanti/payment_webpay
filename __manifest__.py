# -*- coding: utf-8 -*-

{
    'name': 'Webpay Payment Acquirer',
    'category': 'Accounting',
    'author': 'Daniel Santibáñez Polanco',
    'summary': 'Payment Acquirer: Webpay Implementation',
    'website': 'https://globalresponse.cl',
    'version': "4.0.3",
    'description': """Webpay Payment Acquirer""",
    'depends': [
                'payment',
                'payment_currency',
            ],
        'external_dependencies': {
            'python':[
                'urllib3',
                'transbank',
        ],
    },
    'data': [
        'views/webpay.xml',
        'views/payment_provider.xml',
        'views/res_config_settings.xml',
        'views/payment_transaction.xml',
        'data/webpay.xml',
    ],
    'post_init_hook': 'post_init_hook',
    'uninstall_hook': 'uninstall_hook',
    'installable': True,
    'application': True,
}
